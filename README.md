# A collection of pre-commit hooks

This repository contains git hook definitions for use with
[pre-commit](http://pre-commit.com/).

## List of hooks

### Style checkers / formatters

| id                | name                                                                                      |
| ----------------- | ----------------------------------------------------------------------------------------- |
| style-java        | Check/fix Java style ([google-java-format](https://github.com/google/google-java-format)) |
| style-javascript  | Check/fix JavaScript style ([javascript-standard-style](https://standardjs.com/))         |
| style-kotlin      | Check/fix Kotlin style ([ktlint](https://ktlint.github.io/))                              |
| style-kotlin-kfmt | Check/fix Kotlin style ([ktfmt](https://github.com/facebookincubator/ktfmt))              |
| style-sql         | Check/fix SQL style ([sql-formatter-plus](https://github.com/kufii/sql-formatter-plus))   |
| style-typescript  | Check/fix TypeScript style ([google-typescript-style](https://github.com/google/gts))     |

### Linters

| id                                     | name                                         |
| -------------------------------------- | -------------------------------------------- |
| lint-makefile-suspicious-continuations | Check Makefiles for suspicious continuations |
| lint-makefile-suspicious-indents       | Check Makefiles for suspicious indents       |

### Commit message preparation and linting

| id                 | name                                                                                      |
| ------------------ | ----------------------------------------------------------------------------------------- |
| commit-msg-check   | Check commit message ([commitlint](https://github.com/conventional-changelog/commitlint)) |
| commit-msg-restore | Restore previously rejected commit message                                                |

## Configuration of pre-commit

Example `.pre-commit-config.yaml`:

```yaml
repos:
  - repo: https://gitlab.com/tbcs/pre-commit-hooks
    rev: "da39a3e"
    hooks:
      - id: style-java
      - id: commit-msg-restore
      - id: commit-msg-check
```

## Configuration of specific hooks

### commitlint

Example `.commitlintrc.yml` for use with hook `commitlint`:

```yaml
---
# ------------------------------------------------------------------------------
# Configuration for commitlint, extending the conventional commits shared
# configuration:
# https://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-conventional
# ------------------------------------------------------------------------------

scope-list: &scope-list
  - PLACEHOLDER_SCOPE_TO_REJECT_INVALID_SCOPES
type-list: &type-list ## conventional commit types
  - chore
  - ci
  - feat
  - fix
  - docs
  - style
  - refactor
  - perf
  - test
  - revert
  ## custom types
  # - cfg
extends:
  - "@commitlint/config-conventional"
rules:
  scope-enum: [2, always, *scope-list]
  type-enum: [2, always, *type-list]
```
